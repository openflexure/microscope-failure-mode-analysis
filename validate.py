#! /usr/bin/env python3

"""
Short script to validate that the failure mode analysis CSV files match the expected data
schema.

This also produces a little website to view the analysis.

(C) Julian Stirling
Released under the MIT reciprocal license.
"""

import json
import csv
import re
import sys
import os
import shutil
from markdown import markdown
from jsonschema.exceptions import ValidationError as JSValidationError
from pycsvschema.checker import Validator
from pycsvschema.exceptions import ValidationError
from jinja2 import Environment, FileSystemLoader

PUB_DIR = "public"

def valid_primary_keys(primary_keys):
    """Check primary keys are sorted and sequential."""

    # Note that the keys are already validated against a regex on load so
    # we can now just pull out the numbers
    numeric_keys = [int(re.findall(r'\d+', key)[0]) for key in primary_keys]
    #We expect a sequential list starting at 1
    expected_numeric_keys = list(range(1, len(numeric_keys)+1))

    return numeric_keys==expected_numeric_keys


class FailureAnalysisWorksheet:
    """Basic class for a failure mode analysis worksheet"""

    def __init__(self, schema_filename, csv_filename):
        self._schema_filename = schema_filename
        with open(schema_filename, 'r') as schema_file:
            self._schema = json.load(schema_file)
        self._csv_filename = csv_filename
        self._csv_dialect = self._validate()
        self._headers, self._data = self._process_fma()

    @property
    def schema(self):
        """
        Return the CV schema for the failure mode analysis
        """
        return self._schema

    @property
    def data(self):
        """
        Return the data for the failure mode analysis
        """
        return self._data

    @property
    def headers(self):
        """
        Return the headers for the failure mode analysis
        """
        return self._headers

    def _validate(self):
        """
        Validate the CSV file. Will sys.exit on validation fail.
        Returns the csv dialect.
        """
        try:
            validator = Validator(csvfile=self._csv_filename,
                                  schema=self.schema)
        except JSValidationError as error:
            print(f"Error parsing the CSV schema: {self._schema_filename}.")
            print(str(error).split("\n")[0])
            #Hard exit to give a clear message rather than a stack trace.
            sys.exit(2)

        try:
            validator.validate()
        except ValidationError as error:
            print("The input csv file does not validate to the schema.")
            print(error)
            #Hard exit to give a clear message rather than a stack trace.
            sys.exit(1)
        return validator.csv_dialect

    def _process_fma(self):
        data = []
        primary_keys = []
        with open(self._csv_filename, 'r') as csv_file:
            csv_reader = csv.reader(csv_file, dialect=self._csv_dialect)
            for i, row in enumerate(csv_reader):
                if i==0:
                    headers = row
                else:
                    data.append(row)
                    primary_keys.append(row[0])

        if not valid_primary_keys(primary_keys):
            print("IDs should be sorted and sequential")
            sys.exit(1)

        return headers, data

    def insert_calculated_column(self, index, header, function):
        """
        Insert a column of data that is calculated. The function should accept the list
        of the current data the list of headers and return the object to go in the new column.
        """
        for row in self._data:
            row.insert(index, function(row, self.headers))
        self._headers.insert(index, header)

    def get_html_page(self):
        """
        Return an HTML page for this worksheet.

        This page includes a description of the worksheet, the worksheet itself and
        descriptions of the feilds. The descriptions are taken from the schema.
        """

        loader = FileSystemLoader(['templates'])
        env = Environment(loader=loader, trim_blocks=True)
        tmpl = env.get_template("fma.jinja")

        file_description, column_descriptions = self._html_descriptions()
        html = tmpl.render(title=self.schema['title'],
                           file_description=file_description,
                           headers = self.headers,
                           data = self.data,
                           column_descriptions=column_descriptions,
                           csv_file=self._csv_filename)
        return html

    def _html_descriptions(self):
        """Return the descriptions of the file and the columns as HTML"""
        file_description = markdown(self.schema['description'])
        column_md = ""
        for field in self.schema['fields']:
            column_md += "* **"+ field['name'] + "** - " + field['description'] + "\n"
        column_descriptions = markdown(column_md)
        return file_description, column_descriptions


def _public_dir(filename):
    """
    Return the input file name with the public directory prepended
    """
    return os.path.join(PUB_DIR, filename)

def severity_times_chance(row, headers):
    """Calculate severity*chance It would be good to have a way to specify this in the schema
    rather than here!"""
    sev_index = headers.index("Severity of failure")
    chance_index = headers.index("Chance of failure before acceptable time")
    return int(row[sev_index]) * int(row[chance_index])

def main():
    """
    Validate both worksheets and then generate HTML pages for both of them.
    """

    if not os.path.exists(PUB_DIR):
        os.mkdir(PUB_DIR)

    manuf_sheet = FailureAnalysisWorksheet(schema_filename="manufacturing_schema.json",
                                           csv_filename="manufacturing_failures.csv")

    with open(_public_dir("manufacturing.html"), 'w') as html_file:
        html = manuf_sheet.get_html_page()
        html_file.write(html)
    shutil.copyfile("manufacturing_failures.csv", _public_dir("manufacturing_failures.csv"))

    use_sheet = FailureAnalysisWorksheet(schema_filename="use_schema.json",
                                         csv_filename="use_failures.csv")

    #Find the index of the mitigation column and insert the severity*chance column at this location
    mitigation_index = use_sheet.headers.index("Mitigations needed")
    use_sheet.insert_calculated_column(mitigation_index, "Severity*Chance", severity_times_chance)

    with open(_public_dir("use.html"), 'w') as html_file:
        html = use_sheet.get_html_page()
        html_file.write(html)

    shutil.copyfile("use_failures.csv", _public_dir("use_failures.csv"))


if __name__ == "__main__":
    main()
