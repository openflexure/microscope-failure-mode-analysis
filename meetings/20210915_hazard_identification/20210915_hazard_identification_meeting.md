# Hazard identification meeting
Date: 15/9/2021, 0930
Present: William Wadsworth, Joe Knapper, Richard Bowman (note taker), Julian Stirling (chair)

## Objectives
Start identifying hazards with a group of people familiar with the project.  We intend to set up our processes and data formats initially, and will include clinical input and input from our manufacturers in Tanzania in due course.  Today's exercise is not intended to produce an exhaustive use of hazards.


## Initial identification of hazards and characteristics related to safety
*We agreed to work through the questions defined in PD CEN ISO/TR 24971:2020 Annexes A.2 and H, to prompt the identification of hazards.  These hazards will be listed below, and in due course will be converted into a formal table with hazard IDs, etc.*

*We are systematically answering all relevant questions, but are not claiming to have exhaustively identified the hazards arising.*

### A.2.1 What is the intended use and how is the medical device to be used?
We do not yet have an intended use statement for the microscope, but there are some key assumptions that we should document at this point:

* We are considering the case of the microscope as a medical device, i.e. manufactured by a local producer such as BTech and sold to a healthcare facility.  This explicitly excludes kit sales.

* The microscope is intended for use with 20x-100x objectives for high magnification work.  Use of lower magnification objectives is reasonably forseeable misuse.

  **HAZARD:** lower magnifications may produce low quality images

* The microscope is intended for transmission bright-field imaging.  Any other imaging modes are excluded from this exercise.

* The microscope design may be adapted by the legal manufacturer, who is also responsible for their specific statement of intended use.

  **HAZARD:** downstream manufacturers may propose unsuitable intended use statements

* The microscope is intended to be used by trained healthcare professionals, in static or mobile clinic settings.  It is not intended for use by patients.

* The microscope is intended to be integratable (by the manufacturer) with existing hospital data management systems, though this is not a requirement for its operation.

* The microscope must function properly in the Global South, and as such should be robust to high humidity, wide temperature ranges and dust as typically found in clinics there.

  **HAZARD:** Operation in high temperatures

  **HAZARD:** Operation in low temperatures

  **HAZARD:** Operation in high humidity

  **HAZARD:** Operation in dusty environments

* The microscope is intended to be able to be manufactured and maintained in a low-resource setting.  This has implications for the procurement of components and spare/replacement parts, among other issues.

  **HAZARD:** Poor quality components are used, or incorrect parts are substituted.

* Mains power should not be assumed; the need to operate using battery power should be considered.

  **HAZARD:** The microscope's 5V power supply may not be exactly 5V or may be noisy.

* The microscope is a general purpose laboratory instrument, i.e. an *in-vitro* diagnostic device that can be used as part of a variety of procedures.  

  **NEED INPUT:** Understand all reasonably forseeable uses of a diagnostic microscope so that we can better pose the limitations of what it's capable of doing.

* The microscope is intended to scan a microscope slide and produce a digital representation of that sample.  This is typically used in haematology, histopathology, parasitology, or cytology.

* The microscope could in principle be used to observe moving or fixed samples, however it has been optimised for use on fixed samples. 

  **HAZARD:** Motion artefacts or blur lead to incorrect diagnosis e.g. of sperm samples.

* The microscope does not have eyepieces and produces images using a digital image sensor.  The microscope includes software as an essential part of its operation and could be connected to a network.

  **HAZARD:** Security breach of microscope software.  Currently very easy due to fixed default passwords.

  **HAZARD:** Malfunction due to network environment.

  **HAZARD:** Loss or corruption of data.

*There was a brief break from 1100-1120*

### A.2.2 Is the medical device intended to be implanted?

No

### A.2.3 Is the medical device intended to be in contact with the patient or other persons?

The device is not intended to come into contact with the patient.

Technicians will touch the microscope to load samples, power the system on/off, and connect cables and peripherals.

**HAZARD:** Sharp edges.

**HAZARD:** Contamination of contact points or outer casing.

**HAZARD:** Cross-contamination between, or damage of, patient samples.

**HAZARD:** Ingress of fluid, e.g. immersion oil, liquid samples.

### A.2.4 What materials or components are utilized in the medical device or are used with, or are in contact with, the medical device?

A non-exhaustive list of materials:

* Polylactic Acid (parts printed by fused filament fabrication)
* Electronics boards
* Viton O rings
* Steel (stainless or black oxide coated) screws, nuts, washers, etc.
* Brass nuts
* Glass, cement, metal in optical components including microscope objective
* Glass microscope slide
* Immersion oil
* PMMA condenser lens
* Diffuser material
* Computer peripherals (keyboard, monitor, mouse)
* Motors
* LED
* Lubricant oil
* Wires
* Thread lock compound (fixing small gears)
* Rubber feet
* Outer casing (currently left to individual manufacturers)

Materials and components to be eliminated from the medical device design
* Solder (except as part of the electronic circuit boards).
* Tape 

**HAZARD:** Operator in contact with immersion oil.

**HAZARD:** Immersion oil ingress (e.g. to microscope objective, optics module, electronics).

**HAZARD:** Immersion oil contact with PLA components.

**HAZARD:** Sample fluids enter the microscope.

**HAZARD:** Water contact with electronics boards.

**HAZARD:** Wires caught in gears.

**HAZARD:** Corrosion of metal parts.

**HAZARD:** Degradation of optical components due to temperature.

**HAZARD:** Softening of PLA components due to temperature.

**HAZARD:** Degradation of PLA components due to humidity.

**HAZARD:** Mechanical failure of PLA components.

**HAZARD:** Overheating of electronics.

**HAZARD:** Overheating of motors.

**HAZARD:** Degradation of the microscope due to cleaning.

**HAZARD:** Mechanical damage to casing due to impact.

### A.2.5 Is energy delivered to or extracted form the patient?

No

### A.2.6 Are substances delivered to or extracted from the patient?

No

### A.2.7 Are biological materials processed by the medical device for subsequent reuse, transfusion, or transplantation?

No

### A.2.8 Is the medical device supplied sterile or intended to be sterilized by the user, or are other microbiological controls applicable?

The device is not supplied sterile and will not be sterilized by the user or operator.

Consideration should be given to mould growth on optical components, or contamination from samples.

**HAZARD:** Mould growth on glass surfaces.

**HAZARD:** Biological contamination of the microscope from samples.

### A.2.9 Is the medical device intended to be routinely cleaned and disinfected by the user?

Yes - daily or more frequent (between samples) cleaning will be required.

**NEED INPUT:** What parts would need to be cleaned, how frequently, and which cleaning products are used in typical labs.

### A.2.10 Does the medical device modify the patient environment?

No

### A.2.11 Are measurements taken?

Digital images are acquired and processed, but these are not converted into quantitative or qualitative parameters for diagnosis.

Image processing and stage movement both involve calibration, in order to produce uniformly-illuminated images with faithful colour representation.  In the case of samples requiring composite images from multiple fields of view, the stitching algorithm is also important to the integrity of the images.  

**HAZARD:** Composite images do not cover the required area due to errors in stage movement or motor calibration.

**HAZARD:** Images have distorted or incorrect colour representation due to incorrect calibration.

**HAZARD:** Images are not properly focused.

**HAZARD:** Artefacts in images due to damage or dirt in the optics module.

**HAZARD:** Artefacts in images are not noticed due to being hidden by calibration steps (e.g. flat fielding).

**HAZARD:** Condenser numerical aperture is not large enough (e.g. due to misalignment).

### A.2.12 Is the medical device interpretative?

No.  We do not include any interpretative elements in our software.

**HAZARD:** Interpretative functions (including the extraction of quantitative or qualitative diagnostic measurements) are added to the software.

### A.2.13 Is the medical device intended for use in conjunction with other medical devices, medicines or other medical technologies?

This device is intended to produce images that are analysed by a human technician.  We do not recommend any automated analysis software, however it is reasonably forseeable that technicians or manufacturers may choose to use the microscope in conjunction with image analysis software.

**HAZARD:** Poor quality images (that would be noticed by a technician) result in misdiagnosis in connected software.

Sample preparation is the same as for any laboratory microscope, and so involves glass microscope slides, coverslips, and/or other sample chambers.  Some protocols require reagents including but not limited to stains, deionised water and ethanol.  Slides may be prepared using other medical devices such as automated smearing systems, however these are not specific to our microscope and may be used with any laboratory microscope.

**HAZARD:** Sharp edges from broken glass.

**HAZARD:** Image artefacts from sample chambers.

**HAZARD:** Poor quality images due to viewing samples through glass that is too thick (e.g. microscope slide rather than coverslip).

**HAZARD:** Poor quality images due to incorrect reagents, or concentrations of reagents, used in the sample preparation process.

### A.2.14 Are there unwanted outputs of energy or substances?

The microscope includes an onboard computer, including wireless antennae (WiFi, Bluetooth) and communication busses (CSI, Ethernet, USB).

**HAZARD:** Emission of electromagnetic radiation.

Noise and vibration are created by the motors at a very low level.

Motors and electronics generate heat.  

**HAZARD:** High surface temperatures on the motor casings and electronic board.

**HAZARD:** Failure of PLA components due to overheating of electronic parts.

Cleaning of components is required during assembly to remove dust, but this would not be harmful to the user.

**HAZARD:** Residual dust affects imaging performance.

**NEED INPUT:** Are there manufacturing by-products in the objective?

### A.2.15 Is the medical device susceptible to environmental influences?

The microscope contains an embedded computer and custom motor control board.

**HAZARD:** The electronics boards are affected by external electromagnetic fields.

The microscope may be used in wide temperature ranges, high humidity, or dusty environments.

**HAZARD:** Failure of optics or PLA due to temperature, humidity, dust.

The microscope will be transported, and may be transported routinely e.g. in a mobile clinic.  The microscope may be operated in environments where there is some vibration.

**HAZARD:** Failure of mechanical components due to shock or vibration during transport.

**HAZARD:** Malfunction of the microscope due to vibrations in the operating environment.

The microscope may be powered from a variety of power supplies, including dirty or unstable AC power, batteries or solar power.

**HAZARD:** Malfunction or damage resulting from noisy or incorrect power.

The microscope may be operated in environments where there is light, including strong sunlight.

**HAZARD:** Poor quality images due to stray light.

**HAZARD:** Degradation of casing or components due to UV exposure.

The microscope may be subject to spills of common lab fluids.

**HAZARD:** Degradation of casing or components due to spills.

**NEED INPUT:** Enumerate solvents and reagents to test for compatibility.

### A.2.16 Does the medical device influence the environment?

Influences on the environment are limited to those listed under A.2.14.

### A.2.17 Does the medical device require consumables or accessories?

The microscope will almost certainly be supplied with a power supply and/or battery, as appropriate to the application.  This is likely to vary between manufacturers and regions, and may be changed by the end user.

**HAZARD:** Inappropriate or poor quality power supply is used.

The microscope requires computer peripherals, including a keyboard, monitor, and mouse, for its operation.

**HAZARD:** The system is used with a monitor that does not correctly reproduce colour.

**NOTE:** Purchasing certified monitors may be impossible in some locations, and it is reasonably forseeable that monitors may be replaced by the user.  We may need to consider making provision for evaluating available monitors e.g. using test images.

**HAZARD:** The system is used with a monitor that is not able to be viewed in the environment where it is used (e.g. inappropriate size, brightness, viewing angle, or resolution).

**HAZARD:** The system is used with broken or difficult to use input devices.

**HAZARD:** The system is used with input devices that are labelled incorrectly (e.g. incorrect language on keyboard).

Sample preparation requires reagents and consumables, and may involve accessories.  As noted in A.2.13, such accessories are not specific to this device but are common to any laboratory microscope.  Immersion oil is required for oil immersion objectives.  Hazards relating to these are identified above.

*There was a break for lunch at 1245-1420.*

### A.2.18 Is maintenance or calibration necessary?

Yes.  The microscope will be maintained and recalibrated in a variety of settings.  We note that part of our intended use is for the microscope to be manufactured and used in low-resource settings, and it is therefore important to ensure that components, spare parts, and competent maintenance personnel are available, and that the microscope may be serviced in an environment, and with tools that are available in that context.

Some calibration, maintenance and alignment must be performed by the user.  Other more specialist maintenance (including preventative and responsive) and calibration operations may be performed by a competent person.

**HAZARD:** Maintenance or calibration are performed incorrectly, or not frequently enough.

Spare parts and calibration artefacts are required for some maintenance, repairs, and calibration.

**HAZARD:** Maintenance or calibration may become difficult or be performed incorrectly if spare parts or artefacts are not available, e.g. due to supply chain issues.

Optical calibration requires a sharp edge calibration target.  It is important to place an upper bound on the width of the transition from transparent to opaque.  

Calibration of the camera image to allow distance measurement requires the use of a calibration artefact with known lengths; the microscope as described here is not intended to provide distance measurement for diagnostic purposes.

**NEED INPUT:** Is it desirable or necessary to calibrate the microscope for distance measurement.

Routine maintenance and calibration do not require specialist equipment or substances.  Some repairs may require replacement components or tools, all of which will be available to the manufacturer as they are required for initial production.

Routine maintenance such as lubrication of the screw thread must be performed periodically, and will be specified in a maintenance schedule.

**FURTHER STUDY:** How often is lubrication required.

**HAZARD:** Leadscrews are not lubricated sufficiently often, and jam.

Replacement of flexure hinges and other flexible components must be performed according to a maintenance schedule.

**FURTHER STUDY:** How often should flexures be replaced.

**HAZARD:** The microscope is damaged when a component is replaced.

Recalibration of flat field correction should be initiated by the operator in response to noticing artefacts in images.  It typically changes when the illumination is realigned, or other changes (such as swapping objectives) are made to the optics.

**HAZARD:** Flat field correction is not performed often enough, resulting in poor quality images.

**NOTE:** Flat field correction could be simply verified with a periodic software check.  Documentation in a maintenance schedule is also required.

**HAZARD:** Lenses can be inserted incorrectly.

### A.2.19 Does the medical device contain software?

The microscope includes software that is essential to its operation.  This software is supplied pre-installed, and may be periodically updated.  Software is currently able to be updated by the user and updates are not currently verified.  

**NOTE:** We will perform a systematic review of software aspects of the microscope in the near future.

**HAZARD:** Software is corrupted or compromised during an update.

**HAZARD:** The security of the microscope software is compromised.

**HAZARD:** The software on the device has unauthorised changes.

**HAZARD:** The bugs are introduced due to external dependencies.

The list above is very much not exhaustive and will be extended during a detailed software review.

### A.2.20 Does the medical device allow access to information?

Yes, currently the microscope permits access via SSH, or the use of USB ports for control and data transfer.  The microscope can be operated, including data access, as a network device.  There are not currently security controls on the microscope interface.

**HAZARD:** The microscope software is modified by the user.

**HAZARD:** Data stored on the microscope is accessed by an unauthorised user.

**HAZARD:** Data stored on the microscope is modified by an unauthorised user.

### A.2.21 Does the medical device store data critical to patient care?

The microscope stores a digital representation of patient samples as scanned on microscope slides.  This data is critical to the diagnosis, but is not used automatically or in real time for automated feedback to patient care.

**HAZARD:** Data stored on the microscope may be lost.

**HAZARD:** Writing to the SD card can result in damage, rendering the microscope inoperable and potentially losing data.

### A.2.22 Does the medical device have a restricted shelf life?

The flexure mechanism may degrade over time even when not used, which implies a shelf life of 1-2 years.

**FURTHER STUDY:** Improve our lifetime and fatigue data (current knowledge based on STICLab fatigue testing experiment).

**HAZARD:** The microscope body degrades before it is used, resulting in rapid failure.

### A.2.23 Are there any delayed or long-term use effects?

Fatigue testing has revealed a number of failure modes as a result of long term use.  In particular, wear in the leadscrews and nuts can result in degradation of sample positioning, especially when incorrectly lubricated.  Failures in O rings and flexures have also been observed as a result of months of continuous use.  Data management in the software can currently fill the SD card over time.

**HAZARD:** Positioning performance is impaired, or the microscope is inoperable, due to failure in the mechanical system.

**HAZARD:** SD card fills up and system becomes inoperable.

**HAZARD:** SD card can become very slow as it fills up (data from Joe Knapper).

### A.2.24 To what mechanical forces will the medical device be subjected?

The microscope is small and portable, and thus likely to be moved within and between labs.  Shocks and vibration during transport may be significant.  The load capacity of the XY and Z stages is relatively small.  The XY axis is accessible to knocks without further casing.  As a benchtop device, it is likely to receive minor knocks during normal operation and storage.

**HAZARD:** The Z axis can be broken due to the weight of the microscope objective.

**HAZARD:** Shocks to the moving stage break the XY axis.

Mounting and removal of the microscope slide requires the application of small forces to the moving part of the XY axis.  Changing the objective can result in torque applied to the Z axis, and also force applied to the XY axis mechanism.  Some force is required to adjust the position of the condenser.  Replacing O rings involves significant forces.

**HAZARD:** The XY axis is damaged during sample mounting.

**HAZARD:** The Z axis is damaged when the objective is replaced.

**HAZARD:** The microscope body or other components are damaged during O ring replacement.

**NOTE:** Users transporting the microscope by air have previously removed the objective for transport.

**NOTE:** We intend the microscope will be supplied and used in a protective casing, but this is not yet included in the design.

### A.2.25 What determines the lifetime of the medical device?

The main body may degrade on a timescale of years, especially in high humidity.  Leadscrews and nuts may degrade over time, especially if they are not correctly lubricated.  These parts may be replaced by a competent person.  The overall lifetime of the microscope will be determined by the availability of replacement parts and software updates.

### A.2.26 Is the medical device intended for single use?

No.

### A.2.27 Is safe decommissioning or disposal of the medical device necessary?

Electrical components must be disposed of responsibly (e.g. in compliance with national or international regulations for waste electronics and electronic devices).  Electronics should conform to RoHS regulations when manufactured.  There are no biological hazards or known toxic components inherent to the microscope.

### A.2.28 Does installation or use of the medical device require special training or special skills?

The microscope is intended to be used by microscopy technicians.  Interpretation of the images produced requires the same skills already required by manual microscopes, and should only be done by suitably competent persons, subject to existing quality assurance measures.  Some additional training will be required on the specifics of this microscope, even for existing microscopy technicians.

As a portable instrument, the microscope does not require installation in order to function.  Integration of the microscope (e.g. with hospital information management systems) requires specialist skills and may require software modifications which are only possible for the manufacturer.

Maintenance of the microscope must be done by a competent person, and will be further described in the maintenance schedule.

*Meeting adjourned for a break at 1545-1405.*

### A.2.29 How will information for safety be provided?

Instructions for use will be supplied along with the microscope by the manufacturer.  The user manual, maintenance manual, and other relevant information will be available in digital form from the microscope.  The microscope software already includes some documentation (e.g. the initial "tour" of the interface) and further mechanisms such as pop-up help and step by step procedures are planned.

**HAZARD:** Overuse of pop-up messages leads to important information being ignored.

The manufacturer may need to translate or otherwise tailor the instructions for their particular product, and any changes should be carefully considered as part of their risk management process.

**HAZARD:** Information relevant to safety is translated incorrectly.

Particular organisations or facilities may produce their own Standard Operating Procedures and guidance for the inclusion of safety information should be given in the user instructions.

**HAZARD:** Information relevant to safety is present in the original instructions, but is missing from Standard Operating Procedures at the user's laboratory.

### A.2.30 Are new manufacturing processes established or introduced?

3D printing is a novel technique that is not usually used for the manufacturing of microscopes.  We have validated the performance of 3D printed mechanisms in peer-reviewed publications, and have worked with 3D printed mechanisms since 2015.

**HAZARD:** Manufacturing defects in 3D printed parts.

### A.2.31 Is successful application of the medical device dependent on the usability of the user interface?

**FUTURE SESSION:** The software user interface will be considered in a future session.

The physical interface is different from a conventional microscope.

**HAZARD:** Trained microscopy technicians do not understand how to use this microscope.

**NEED INPUT:** Require usability feedback from clinical users.

**NOTE:** A survey is planned along with the next round of testing microscopes in clinics.

### A.2.32 Does the medical device include an alarm system?

No.

### A.2.33 In what ways might the medical device be misused (deliberately or not)?

**FUTURE SESSION:** We will consider modes of misuse in the future.  Many possible misuses are described above, and could be collected.

### A.2.34 Is the medical device intended to be mobile or portable?

Yes.  Considerations from this section mostly relate to the design of the outer casing, which is currently left to particular manufacturers.  Transport is also considered above.

**HAZARD:** Microscope is damaged when it is moved.

### A.2.35 Does the use of the medical device depend on essential performance?

There are minimum levels of mechanical, computational and optical performance that are required for correct operation.

**HAZARD:** The resolution of the microscope degrades and parasites are no longer visible.

**FUTURE SESSION:** Review definition of "essential performance" from IEC 60601-1.

### A.2.36 Does the medical device have a degree of autonomy?

Scanning a microscope slide is a process taking between 10 and 60 minutes.  Software allows this to be aborted, and reports errors in the process as pop-up warnings in the user interface.  During slide scanning, the microscope's motorised stage will move, though this will not interact with anything outside the outer casing.

**HAZARD:** The microscope fails to scan a slide, and the technician is not aware until much later, causing a delay to diagnosis.

**HAZARD:** The microscope damages the sample during automated scanning/focusing.

**HAZARD:** The automatic calibration routines produce incorrect results.

### A.2.37 Does the medical device produce an output that is used as an input in determining clinical action?

Yes.  Images collected by the microscope will be interpreted by a technician or doctor in order to determine a diagnosis or prognosis, and direct treatment.  

**HAZARD:** Delays in collecting these images may result in delays to diagnosis and subsequent treatment.  

**HAZARD:** Poor quality images may result in incorrect or inconclusive results, delaying or incorrectly informing future treatment.

**HAZARD:** Automated image acquisition produces poor or out-of-focus images, even with correctly functioning hardware.

**HAZARD:** Image data is lost before it has been interpreted, resulting in a delay to diagnosis.

## Conclusion

With the noted exceptions of software and a more extensive consideration of misuse, we have now taken an initial pass through PD CEN ISO/TR 24971 Annex A.2.  We have identified 100 hazards in this document, but more hazards and associated harms remain to be identified.  A more systematic procedure for listing hazards will be developed.  We will also consider the points raised by Annex H, specific to in-vitro diagnostics.

*Meeting ends at 1643.*