# Failure mode analysis for the OpenFlexure Microscope

We are just starting failure mode analysis is is quite incomplete.

We have two seperate work sheets:


* [**Manufacturing**](https://openflexure.gitlab.io/microscope-failure-mode-analysis/manufacturing.html) - for failures that happen and can be detected during the manufacturing process, thus will not affect the end user.
* [**Use**](https://openflexure.gitlab.io/microscope-failure-mode-analysis/use.html) - for failures that occur when the microscope is with the end user.


Both worksheets are written as CSV files are validated against a JSON schema. This allows simple interaction with the data via spreadsheet software, but also allows us to check the data integrity and check the files into Git as text for posterity.
