{
    "title": "OpenFlexure Microscope Failure Mode Analysis: Use",
    "description": "This document describes failures that occur once the microscope has been shipped to the customer. Any failure that could occur during use should be listed here even if the possible cause is due to a manufacturing problem.",
    "exactFields": true,
    "fields": [
        {
            "name": "ID",
            "description": "A record identifier. This should be sequential and never changed.",
            "type": "string",
            "nullable": false,
            "pattern": "USE[0-9]{5}"
        },
        {
            "name": "Failure",
            "description": "A short description of the identified failure.",
            "type": "string",
            "nullable": false
        },
        {
            "name": "Acceptable failure time",
            "description": "A time deemed acceptable for the part to fail after. Where a time specified in months refers to the calendar time, but a time in hours refers to continuous use time.",
            "type": "string",
            "nullable": false
        },
        {
            "name": "How failure is addressed",
            "description": "Summary of how the failure is addressed. i.e. which parts need replacing.",
            "type": "string",
            "nullable": false
        },
        {
            "name": "Who addresses failure",
            "description": "Who addresses the failure. Options are 'Manufacturer', 'Technician', or 'User'. Use manufacturer if the repair requires training or practice, technician if a trained technician can complete the repair with minimal instructions, and user if the fix is a simple and obvious task.",
            "type": "string",
            "enum": [
                "Manufacturer",
                "Technician",
                "User"
            ],
            "nullable": false
        },
        {
            "name": "What is broken or damaged",
            "description": "What is broken or damaged should include damaged parts. The state of the microscope. If samples are damaged, or data is lost. Should also include lost time.",
            "type": "string",
            "nullable": false
        },
        {
            "name": "Severity of failure",
            "description": "How severe the failure is. scale 1-5: 1 lost time only user repair, 2-3 minor maintenance required, 4 major maintenance required from manufacturer, 5 data loss/sample damage",
            "type": "number",
            "minimum": 1,
            "maximum": 5,
            "nullable": false
        },
        {
            "name": "Maintenance schedule",
            "description": "Items in the regular maintenance schedule for the microscope that help mitigate this problem.",
            "type": "string"
        },
        {
            "name": "Other mitigations in place",
            "description": "Mitigations that are already in place (that are not the maintenance schedule) to reduce the likelihood or severity of the failure.",
            "type": "string"
        },
        {
            "name": "Chance of failure before acceptable time",
            "description": "Chance of failure before the specified acceptable time for failure. On a scale from 1-5. Where 1 corresponds to a very rare occurrence, 5 is a frequent occurrence.",
            "type": "number",
            "minimum": 1,
            "maximum": 5,
            "nullable": false
        },
        {
            "name": "Mitigations needed",
            "description": "Further mitigations that are needed to resolve this failure or reduce likelihood. Required for severity*chance>=15.",
            "type": "string"
        },
        {
            "name": "Possible reason for failure",
            "description": "Description of why this failure happens.",
            "type": "string",
            "nullable": false
        },
        {
            "name": "Testing procedure",
            "description": "Description of procedure to test if this failure has occurred. If appropriate describe the acceptable range for test results.",
            "type": "string"
        },
        {
            "name": "Reasoning behind test procedure",
            "description": "Description of why this testing procedure was chosen and how the acceptable range was determined.",
            "type": "string"
        },
        {
            "name": "Record status",
            "description": "The status of this record.",
            "type": "string",
            "enum": [
                "Active",
                "Resolved awaiting release",
                "Resolved",
                "Invalid"
            ],
            "nullable": false
        },
        {
            "name": "Resolution details",
            "description": "Description of resolution for resolved records. This should point to the Git commit or merge request for fixes where possible.",
            "type": "string"
        }
    ]
}
